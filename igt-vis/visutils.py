#
# piglit results.json visualizer over history and hosts
#
# Originally by Tomi Sarvela for Intel OTC lab
#
import os
import sys
import json
import re
import time

import bz2

from collections import OrderedDict
from multiprocessing import Process, Queue
import queue


def err(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


# Just a number to order hosts by their partial name, not real gen
# https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units
# https://en.wikichip.org/wiki/intel/microarchitectures
intel_gpu_gens = [
    ("gdg", 3.0),  # Grantsdale-G
    ("blb", 3.5),  # Bear Lake-B  Gen3 GMA3100
    ("pnv", 3.6),  # Pine View    Gen3 GMA3150
    ("bwr", 4.0),  # Broadwater
    ("brw", 4.0),  # Broadwater   Mesa-spelling
    ("elk", 4.5),  # Eagle Lake   Gen4 GMA4500
    ("ctg", 4.6),  # Cantiga      Gen4 GMA4500m
    ("ilk", 5.0),  # Iron Lake    Gen5
    ("snb", 6.0),  # Sandy Bridge Gen6
    ("ivb", 7.0),  # Ivy Bridge   Gen7
    ("byt", 7.1),  # Bay Trail (ValleyView)
    ("hsw", 7.5),  # Haswell      Gen7.5
    ("bdw", 8.0),  # Broadwell    Gen8
    ("bsw", 8.5),  # Braswell (CherryView)
    ("skl", 9.0),  # Skylake      Gen9
    ("bxt", 9.1),  # Broxton (Apollo Lake, Goldmont)
    ("apl", 9.1),  # Apollo Lake
    ("kbl", 9.5),  # Kaby Lake
    ("glk", 9.6),  # Gemini Lake
    ("cfl", 9.7),  # Coffee Lake
    ("whl", 9.8),  # Whiskey Lake
    ("cml", 9.9),  # Comet Lake   Still 14nm
    ("cnl", 10.0),  # Cannon Lake  Gen10 # 10nm
    ("icl", 11.0),  # Ice Lake     Gen11
    ("ehl", 11.5),  # Elkhart Lake Gen11
    ("tgl", 12.0),  # Tiger Lake   Gen12
    ("dg1", 12.1),  # Discrete Graphics 1
    ("rkl", 12.2),  # Rocket Lake  Gen12 14nm
    ("jsl", 12.3),  # Jasper Lake  Gen12
    ("adl", 12.5),  # Alder Lake         10nm
    ("rpl", 13.0),  # Raptor Lake  Gen13
    ("dg2", 13.1),  # Intel Arc Alchemist
    ("atsm", 13.2),  # Arctic Sound M
    ("mtl", 14.0),  # Meteor Lake
    # Two letter acronyms last
    ("gd-", 3.0),  # Grantsdale   Gen3 GMA900
    ("bw-", 4.0),  # Broadwater   Gen4 GMA3000/X3000
    ("cl-", 4.2),  # Crestline    Gen4 GMAX3100
]

hostaliases = {}


def read_hostalias_file(filename):
    """Read in regex-replace pairs for hostname aliasing"""
    global hostaliases
    try:
        with open(filename, "r") as fp:
            for line in fp.readlines():
                r, s = line.strip().split(" ", 1)
                hostaliases[re.compile(r)] = s
            print(f"Host alias file loaded from: {filename}")
    except (OSError, IOError, UnicodeDecodeError, EOFError) as e:
        raise e
    return hostaliases


def hostalias(hostname):
    """Go through ordered dict of regex matches.
    Use first matching or return None (no matches)"""
    global hostaliases
    for r in hostaliases.keys():
        if r.match(hostname):
            return re.sub(r, hostaliases[r], hostname, count=1)
    return None


def match_resumelog(j, dirname):
    """resume.log contains sequential events, one per line

    Format:
    last i comment # i: last test done (directory exists), starts from 0 just as IGT
    boot filename  # rotate to new bootlog
    dmesg filename # rotate to new dmesg, host not necessarily rebooted
    next i comment # i: next test to be run (assumed next directory)

    Either 'last' or 'next' entries are enough for completeness"""
    # need joblist.txt and resume.log to get reboot points
    try:
        with open(os.path.join(dirname, "resume.log")) as fp:
            resumelog = fp.read().splitlines()
        with open(os.path.join(dirname, "joblist.txt")) as fp:
            joblist = fp.read().splitlines()
    except:
        return

    bootfile = None
    dmesgfile = None
    last = 0
    for line in resumelog:
        item = line.split(" ")
        if item[0].lower() == "last":
            for i in range(last, int(item[1]) + 1):
                t = "igt@" + "@".join(joblist[i].split(" "))
                # print(i, t, bootfile, dmesgfile)
                # if there is no set logfile, drop through to template default
                if bootfile:
                    j["tests"][t]["boot_file"] = bootfile
                if dmesgfile:
                    j["tests"][t]["dmesg_file"] = dmesgfile
            last = int(item[1]) + 1
        elif item[0].lower() == "next":
            for i in range(last, int(item[1])):
                t = "igt@" + "@".join(joblist[i].split(" "))
                # print(i, t, prevbootfile, prevdmesgfile)
                # if there is no set logfile, drop through to template default
                if prevbootfile:
                    j["tests"][t]["boot_file"] = prevbootfile
                if prevdmesgfile:
                    j["tests"][t]["dmesg_file"] = prevdmesgfile
            last = int(item[1])
        elif item[0].lower() == "boot":
            prevbootfile = bootfile
            bootfile = item[1]
        elif item[0].lower() == "dmesg":
            prevdmesgfile = dmesgfile
            dmesgfile = item[1]
    # add the last known logs to the rest of the tests
    for i in range(last, len(joblist)):
        t = "igt@" + "@".join(joblist[i].split(" "))
        # print(i, t, bootfile, dmesgfile)
        if bootfile:
            j["tests"][t]["boot_file"] = bootfile
        if dmesgfile:
            j["tests"][t]["dmesg_file"] = dmesgfile
    return


def readfiles_parallel(q, files=[], combine="shard", resume=None):
    for filename in files:
        j = None
        try:
            if filename[-4:] == ".bz2":
                with bz2.BZ2File(filename) as fp:
                    try:
                        j = json.loads(
                            fp.read().decode("utf-8"), object_pairs_hook=OrderedDict
                        )
                    except:
                        err("Warning: bzip", filename, "wasn't in correct JSON format")
            else:
                with open(filename, "r") as fp:
                    try:
                        j = json.loads(fp.read(), object_pairs_hook=OrderedDict)
                    except:
                        err("Warning: file", filename, "wasn't in correct JSON format")
        except (OSError, IOError, UnicodeDecodeError, EOFError) as e:
            err("Error:", e)
            continue
        if not j:
            continue
        j["filename"] = filename
        # read in necessary files if we try log-matching for resume-run
        if resume:
            match_resumelog(j, os.path.dirname(filename))
        run = None
        try:
            name = j["name"]
        except (KeyError) as e:
            err("Error: no 'name' within file", filename)
            continue
        # make some assumptions about name of testrun
        # note: .*? is non-greedy match
        if re.match(r".*@.*@.*", name):  # host@build@run
            host, build, run = name.split(sep="@")
            try:
                run = int(run)
            except:
                run = None
        elif re.match(r".*?@.*?", name):  # host@build
            host, build = name.split(sep="@")
        elif re.match(r".*?/.*?/\d", name):  # build/host/run
            build, host, run = name.split(sep="/")
            run = int(run)
        elif re.match(r".*?/.*?/.*", filename):  # build/host/results.json*
            build, host = filename.split(sep="/")[-3:-1]
        else:
            build = name
            try:  # pick hostname from uname string
                host = re.match(r".*? (.*?) .*", j["uname"]).group(1)
            except Exception as e:  # didn't find any
                print("Exception:", e)
                continue
        # if there was no run number in run name, try to deduce from path
        if run is None:
            try:
                run = int(re.match(r".*results([0-9]*).json.*", filename).group(1))
            except:
                pass
        if run is None:
            try:
                run = int(re.match(r".*/([0-9]*)/results.json.*", filename).group(1))
            except:
                pass
        if run is None:
            run = 0  # default run ID

        for test in j["tests"]:
            if "result" not in j["tests"][test]:
                continue
            j["tests"][test]["run"] = run

        alias = hostalias(host)
        # combine will be deprecated
        if not alias and combine and combine in host:
            alias = re.match("(.*?)-?[0-9]{1,3}$", host).group(1)
        if alias:
            for test in j["tests"]:
                j["tests"][test]["hostname"] = host
            host = alias
        q.put((build, host, j))


def readfiles(files, combine="shard", resume=None):
    """Reads set of JSON files to array"""

    # '-' (dash) is a marker for stdin filelist
    if "-" in files:
        files.remove("-")
        files.extend([line.strip() for line in sys.stdin])

    jsons = OrderedDict()  # dictionary with tuples (build, host)
    q = Queue()
    ps = set()
    while ps or files:
        # Add workers if files left and not overtaxing the I/O
        # Keep limit reasonable for thread and IO reasons
        if files and len(ps) < 15:
            p = Process(
                target=readfiles_parallel,
                args=(q,),
                kwargs={"files": files[0:40], "combine": combine, "resume": resume},
            )
            ps.add(p)
            p.start()
            files = files[40:]

        # get new data (json) from worker
        try:
            build, host, j = q.get(timeout=1)
            # cleanup from igt_runner
            if not (build, host) in jsons:
                jsons[(build, host)] = j
            else:
                # Invalidate total time if inconsistencies in time_elapsed
                if "time_elapsed" in jsons[(build, host)]:
                    try:
                        jsons[(build, host)]["time_elapsed"]["end"] += (
                            j["time_elapsed"]["end"] - j["time_elapsed"]["start"]
                        )
                    except:
                        del jsons[(build, host)]["time_elapsed"]
                for test in j["tests"]:
                    if "result" not in j["tests"][test]:
                        continue
                    jsons[(build, host)]["tests"][test] = j["tests"][test]
                    try:  # add to total result sums if there is section in jsons
                        jsons[(build, host)]["totals"][""][
                            j["tests"][test]["result"]
                        ] += 1
                    except:
                        pass
        except queue.Empty:
            time.sleep(1)
        except BaseException as e:
            print("readfiles exception:", e)

        # Clean process if completed (dead)
        for p in ps.copy():
            if not p.is_alive():
                p.join()
                ps.remove(p)
    return jsons


def parsetests(jsons, sort=False, collate=None, change=False):
    """Collects all test names from all JSON files"""
    builds = []
    hosts = []
    tests = []
    testset = set()

    for build, host in jsons:
        if build not in builds:
            builds.append(build)
        if host not in hosts:
            hosts.append(host)
        tl = [test for test in jsons[(build, host)]["tests"]]

        if not tests:  # keep order of first testlist
            tests = tl
        else:
            tests.extend(sorted(list(set(tl) - testset), key=lambda x: tl.index(x)))

        testset |= set(tl)

    # Remove hosts without comparison possibility if changes wanted
    # Ickle thinks we might be better off with this data
    # if change:
    #    for host in hosts[:]:
    #        test = []
    #        for build in builds:
    #            try:
    #                if jsons[(build,host)]: test.append(build)
    #            except (KeyError): pass
    #        if len(test) <= 1:
    #            for build in test: del jsons[(build,host)]
    #            hosts.remove(host)

    # Old approach: just blindly hide tests where all results are in collated[]
    if collate and not change:
        for test in tests[:]:
            show = False
            for build in builds:
                for host in hosts:
                    try:
                        if jsons[(build, host)]["tests"][test]["result"] not in collate:
                            show = True
                            break
                    except:  # 'notrun' has no result
                        if "notrun" not in collate:
                            show = True
                            break
                if show is True:
                    break
            if show is False:
                tests.remove(test)

    # New approach: check collated tests also in changed
    # this way we can skip none/notrun-pass changes but show skip-pass
    # if old view is needed, use -C -c skip,pass
    if change:
        for test in tests[:]:
            show = False
            for host in hosts:
                lastres = None
                for build in builds:
                    try:
                        res = jsons[(build, host)]["tests"][test]["result"]
                    except:
                        continue
                    # Always skip notrun results
                    if res == "notrun":
                        continue
                    # Always show incompletes
                    if res == "incomplete":
                        show = True
                        break
                    # show all non-collated results
                    if collate and res not in collate:
                        show = True
                        break
                    # show if result has changed
                    if lastres and res != lastres:
                        show = True
                        break
                    lastres = res
                # If any build result is shown, then break hosts-loop
                if show is True:
                    break
            if show is False:
                tests.remove(test)

    return sorted(tests) if sort else tests


buildorder = []


def readbuildorder(filename):
    global buildorder
    try:
        with open(filename, "r") as f:
            buildorder = [
                line.strip().split(",")[-2]
                if line.strip()[-1] == ","
                else line.strip().split(",")[-1]
                for line in f
            ]
    except:
        print("Couldn't load", filename)


def parsebuildstr(name):
    global buildorder

    if buildorder:
        i = buildorder.index(name) if name in buildorder else 0
        return (None, i)

    # Can be one of:  -v4.14-rc1 -v4.14    -v4.14.0
    # munge to format: 004013901 004013999 004014000
    m = re.match(r"(.*)-v([0-9]*)\.([0-9]*)(-rc([0-9]*)|\.([0-9]*)|$)", name)
    if m:
        buildname = m.group(1)
        ver = int(m.group(2))
        major = int(m.group(3))
        if m.group(4):
            if m.group(4)[0] == "-":  # -rc
                major = major - 1
                minor = int(m.group(5)) + 900
            elif m.group(4)[0] == ".":  # .stable
                minor = int(m.group(6))
            else:
                major = major - 1
                minor = 999
        else:
            minor = 0
        buildnum = minor + 1000 * major + 1000 * 100 * ver
    else:
        # take rightmost continuous digitstring as a number
        try:
            buildnum = re.sub("[^0-9]", " ", name).split()[-1]
        except:
            buildnum = "1"
        # take buildnum out from the string, leaving name
        try:
            buildname = name.replace(buildnum, "")
        except:
            buildname = "Build"
    return (buildname, int(buildnum))


def grouphosts(tuple):
    """Ordering of tuple (gen, host, buildname, buildrun)"""
    host = tuple[1]
    buildname, buildnum = parsebuildstr(tuple[0])
    g = -100
    for gen in intel_gpu_gens:
        if gen[0] in host:
            g = gen[1]
            break
    return (-g, host, buildname, -buildnum)


def groupbuilds(tuple):
    """Ordering of tuple (buildname, buildrun, gen, host)"""
    host = tuple[1]
    buildname, buildnum = parsebuildstr(tuple[0])
    g = 100
    for gen in intel_gpu_gens:
        if gen[0] in host:
            g = gen[1]
            break
    return (buildname, -buildnum, -g, host)
