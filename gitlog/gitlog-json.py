#!/usr/bin/env python3
#
# gitlog-json.py
#
# Creates JSON with git log information in gitlog.html digestible form
# Git log format preferred:
#
# git log --format=medium --decorate=short --no-abbrev-commit
#

import argparse
import datetime
import json
import re
import sys


def parse_args():
    """Command line parser"""
    ap = argparse.ArgumentParser(description="gitlog-to-JSON packer")
    ap.add_argument(
        "-i", "--inputfile", default=None, help="Read gitlog from file. Default stdin"
    )
    ap.add_argument(
        "-o", "--outputfile", default=None, help="Write JSON to file. Default stdout"
    )
    return ap.parse_args()


def printerr(*args, **kwargs):
    """Shortcut for printing error message"""
    print(*args, file=sys.stderr, **kwargs)


def readlog(fp):
    """Line-by-line git log to dict parsing. Log format expectation:
    ^commit 123 (refs)$
    ^Author: email$
    ^rest of the headers$
    ^$
    ^    Description-maybe-header$
    ^    $
    ^    Line1$
    ^    Line2$
    ^$"""
    log = {
        "commits": [],
        "refs": {},
        "header": {},
        "subject": {},
        "text": {},
        "created": f"{datetime.datetime.utcnow().replace(microsecond=0).isoformat()}",
    }
    re_commit = r"^commit ([0-9a-f]+) ?(.*)?(.*)"
    while line := fp.readline():
        if not line.startswith("commit "):
            continue

        match = re.match(re_commit, line)
        commit = match.group(1)
        log["commits"].append(commit)

        tags = match.group(2).split(",")
        if tags[0]:
            log["refs"][commit] = [
                tag.strip("() ").replace("tag: ", "", 1) for tag in tags
            ]

        log["header"][commit] = ""
        while line := fp.readline():
            if line == "\n":
                break
            log["header"][commit] += line

        text = ""
        while line := fp.readline():
            if line == "\n":
                break
            text += line[4:]
        if match := re.match(r"^([^\n]*)\n\n", text):
            log["subject"][commit] = match.group(1)
            text = text[len(match.group(1)) + 2 :]
        log["text"][commit] = text

    return log


# MAIN
if __name__ == "__main__":
    args = parse_args()

    # read input line by line instead of slurping it into memory
    if args.inputfile:
        try:
            fp = open(args.inputfile, "r")
        except:
            printerr(f"File {args.inputfile} open error")
            exit(1)
    else:
        fp = sys.stdin

    try:
        log = readlog(fp)
    except KeyboardInterrupt:
        printerr(f"Interrupted")
        exit(1)

    fp.close()

    if args.outputfile:
        try:
            open(args.outputfile).write(json.dumps(log).encode("utf-8"))
        except:
            printerr(f"File {args.outputfile} write error")
            exit(1)
    else:
        print(json.dumps(log))
